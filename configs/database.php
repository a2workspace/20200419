<?php

return [
    'host' => 'localhost',
    'port' => '3306',
    'charset' => 'utf8',
    'database' => 'web_class',
    'username' => 'web_class',
    'password' => '$2y$10$Ohdd6Pt.nmYAJMgzDWl38.ZdQ',
];
