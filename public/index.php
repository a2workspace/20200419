<?php

require __DIR__ . '/../vendor/autoload.php';

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Sample\Facades\DB;

$config = require __DIR__ . '/../configs/database.php';
DB::connect($config);

// =============================================================================
// = Define our routes.
// =============================================================================

$app = AppFactory::create();

$app->get('/', function (Request $request, Response $response, $args) {
    include __DIR__ . '/app.html';
    return $response;
});

$app->get('/users', function (Request $request, Response $response, $args) {
    $data = DB::fetchAll('users');
    
    $response->getBody()->write(json_encode($data));
    return $response->withHeader('Content-Type', 'application/json');
});

$app->get('/users/{id}', function (Request $request, Response $response, $args) {
    $data = DB::find('users', $args['id']);
    
    $response->getBody()->write(json_encode($data));
    return $response->withHeader('Content-Type', 'application/json');
});

$app->run();
